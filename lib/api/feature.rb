require_relative "client"

module GitLab
  module API
    class Features
      def initialize(features)
        @features = features
      end

      def to_a
        @features.map(&:to_h)
      end
    end

    Feature = Struct.new(:name, :state, :gates) do
      def gate_list
        gates.map { |gate| "#{gate['key']}: #{gate['value']}" }.join(", ")
      end

      def to_h
        super.merge(gate_list: gate_list)
      end

      def to_a
        [to_h]
      end
    end

    class FeatureClient
      def initialize(client = Client.new)
        @client = client
      end

      def list
        response = @client.get("/api/v4/features")

        if response.code.to_i != 200
          fail Cog::Error.new("Failed to list features: #{response.code} - #{response.message}")
        end

        features = JSON.parse(response.body).map { |f| Feature.from_hash(f) }

        Features.new(features)
      end

      def set(name, value)
        response = @client.post("/api/v4/features/#{name}", value: value)

        if response.code.to_i != 201
          fail Cog::Error.new("Failed to set feature #{name}: #{response.code} - #{response.message}")
        end

        feature = JSON.parse(response.body)

        Feature.from_hash(feature)
      end
    end
  end
end
