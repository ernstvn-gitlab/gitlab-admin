require_relative "helper"
require "api/broadcast"

module CogCmd
  module Gitlab
    class Broadcast < Cog::Command
      def run_command
        message = Args.required(request.args[0], "Can't set broadcast without a message")
        time_start = request.options["start"]
        time_end = request.options["end"]
        response.content = GitLab::API::BroadcastClient.new.add(message, time_start, time_end)
      end
    end
  end
end
