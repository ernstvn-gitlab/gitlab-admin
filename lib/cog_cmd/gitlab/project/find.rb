require_relative "../helper"
require "api/project"

module CogCmd
  module Gitlab
    module Project
      class Find < Cog::Command
        def run_command
          project_path = Args.required(request.args[0], "I need a project path to perform a search")
          GitLab::API::ProjectClient.new.find(project_path) do |project|
            response.template = "project_find"
            response.content = [project.to_h]
          end
        end
      end
    end
  end
end
