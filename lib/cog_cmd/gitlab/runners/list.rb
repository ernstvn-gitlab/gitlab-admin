require_relative "../helper"
require "api/runners"

module CogCmd
  module Gitlab
    module Runners
      class List < Cog::Command
        def run_command
          runners = GitLab::API::RunnersClient.new.list
          response.template = "runners_list"
          response.content = runners.to_a
        end
      end
    end
  end
end
