require 'spec_helper'
require 'cog_cmd/gitlab/user/lastevent'
require 'cog_cmd/gitlab/user/events'


describe "events" do
  before { allow(STDIN).to receive(:tty?) { true } }

  let(:user) {
    {
      id: 1,
      username: "myuser",
    }
  }

  let(:event) {
    {
      created_at: "yesterday",
      action_name: "created",
      target_type: "note",
      target_title: "Some Content"
    }
  }

  it "can find no events" do
    reqs = []
    reqs << (stub_request(:get, /api\/v4\/users\?username=.*/).
      with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
      to_return lambda { |request| { status: 200, body: [user].to_json } })
    reqs << (stub_request(:get, /api\/v4\/users\/1\/events\?per_page=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 404 ))
    with_environment("myuser") {
      finder = CogCmd::Gitlab::User::Lastevent.new
      finder.run_command
      expect(finder.response.content).to be_empty
    }

    reqs.each { |req| remove_request_stub(req) }
  end

  it "can find at least 1 event" do
    reqs = []
    reqs << (stub_request(:get, /api\/v4\/users\?username=.*/).
      with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
      to_return lambda { |request| { status: 200, body: [user].to_json } })
    reqs << (stub_request(:get, /api\/v4\/users\/1\/events\?per_page=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda { |request| { status: 200, body: [event].to_json } } )
    with_environment("myuser") {
      finder = CogCmd::Gitlab::User::Events.new
      finder.run_command
      expect(finder.response.content[0].to_h).to eq(event)
    }

    reqs.each { |req| remove_request_stub(req) }
  end
end
